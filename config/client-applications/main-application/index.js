// Note: RESOURCE ACCESS URL IS RELATIVE TO PUBLIC DIRECTORY

module.exports = {
  accessUrl : '/',
  indexFileUrl : '/views/main.jade',
  metaData : {
  	title : 'University Events',
  	description : 'My-project-description',
  	keywords : 'tag1, tag2, tag3',
  	siteName : 'site_name',
  	appID : 'AppID',
  	url : 'Url',
  	// favicon : '/img/favicon.ico',
  	image : '/img/image.png',
  	type : 'website'
  },
  angular : {
    globalModlueName : 'My-AdminProject',
    globalDependencies : ['ui.router', 'ui.bootstrap','ui.router.stateHelper','ngCookies'],
    modules : ['home'],// ['core', 'LR', 'home'],
  },
  css : {
    vendor : [
      '/lib/bower/bootstrap/dist/css/bootstrap.min.css',
      '/lib/bower/bootstrap/dist/css/bootstrap-theme.min.css',
      '/lib/bower/font-awesome/css/font-awesome.min.css'
    ],
    custom : [
    '/style-sheets/common.css',
    ]
  },

  js : {
    vendor : [
      '/lib/bower/lodash/dist/lodash.min.js',
      '/lib/bower/jquery/dist/jquery.min.js',
      // '/lib/bower/angular/angular.min.js',
      '/lib/bower/angular/angular.js',
      '/lib/bower/angular-bootstrap/ui-bootstrap-tpls.min.js',
      '/lib/bower/angular-cookies/angular-cookies.min.js',
      '/lib/bower/angular-ui-router/release/angular-ui-router.min.js',
      '/lib/bower/angular-ui-router.stateHelper/statehelper.min.js'

    ],
    custom : [
    ],
    test : [
      '/lib/bower/angular-mocks/angular-mocks.js',
      '/modules/*/tests/*.js'
    ]
  }
};