'use strict';

module.exports = {
    db: 'mongodb://localhost/university-dev',
    app: {
        title: 'University - Dev Mode'
    },
    s3: {
        // "accessKeyId": "",
        // "secretAccessKey": "",
        // "region": "",
        // "bucketName": '',
        // "awsAccountId": ''
        // 'acl': 'public-read'
    },
    ses: {
        // "accessKeyId": "",
        // "secretAccessKey": "",
        // "serviceUrl": ''
    },
    esPrefix: 'dev' //Elastic search index prefix
};