'use strict';

module.exports = {
    db: 'mongodb://localhost/university-dev',
    s3: {
        // "accessKeyId": "",
        // "secretAccessKey": "",
        // "region": "",
        // "bucketName": '',
        // "awsAccountId": ''
    },
    ses: {
        // "accessKeyId": "",
        // "secretAccessKey": "",
        // "serviceUrl": ''
    },
    esPrefix: '' //Elastic search index prefix
};