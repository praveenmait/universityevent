'use strict';

module.exports = {
    db: 'mongodb://localhost/temp-dev-test',
    port: 3001,
    app: {
        title: 'University - Test Mode'
    },
    s3: {
    },
    ses: {
    },
    esPrefix: 'test' //Elastic search index prefix
};