'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config/config'),
    ObjectId = Schema.ObjectId,
    crypto = require('crypto');


// Acitvity Schema

var Activity = new Schema({
    university: {
        type: ObjectId,
        ref: "University"
    },
    events: {
        type: ObjectId,
        ref: "Event"
    },
    participants: [{
        type: ObjectId,
        ref: "Participants"
    }],
    startDate : Date,
    endDate : Date
});

Activity.plugin(require('mongoose-timestamp'));

var Activity = mongoose.model('Activity', Activity);
