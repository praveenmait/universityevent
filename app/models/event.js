'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config/config'),
    ObjectId = Schema.ObjectId,
    async = require('async'),
    crypto = require('crypto');

// Event Schema

var EventSchema = new Schema({
    university: String,
    name: String,
    startime: Date,
    endtime: Date,
    description: Schema.Types.Mixed,
    location: Schema.Types.Mixed,
    participants: {
        type: ObjectId,
        ref: "Participants"
    },
    user:{
        type: ObjectId,
        ref: "User"
    },

});

EventSchema.plugin(require('mongoose-timestamp'));
var Event = mongoose.model('Event', EventSchema);