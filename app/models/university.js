'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config/config'),
    ObjectId = Schema.ObjectId,
    async = require('async'),
    crypto = require('crypto');

// University Schema

var UniversitySchema = new Schema({
    name: {
        type:String,
        required: true,
        unique: true
    },
    description: Schema.Types.Mixed,
    location: Schema.Types.Mixed,
    user:{
        type: ObjectId,
        ref: "User"
    },
    events:[{
        type: ObjectId,
        ref: "Event"
    }]

});

UniversitySchema.plugin(require('mongoose-timestamp'));
var University = mongoose.model('University', UniversitySchema);