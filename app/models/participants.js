'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config/config'),
    ObjectId = Schema.ObjectId,
    async = require('async'),
    crypto = require('crypto');

// Participant Schema

var ParticipantSchema = new Schema({
    name: String,
    institute: {
        type: ObjectId,
        ref: "University"
    },
    age: Number,
    events:[{
        type: ObjectId,
        ref: "Event"
    }]

});

ParticipantSchema.plugin(require('mongoose-timestamp'));
var Participants = mongoose.model('Participants', ParticipantSchema);