var universityCtrl = require('../controllers/universityCtrl');


module.exports = function(app) {
    app.post('/university', universityCtrl.create)
    app.get('/university', universityCtrl.list)
    app.get('/university/:uid', universityCtrl.getItem)
};