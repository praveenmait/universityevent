var participantCtl = require('../controllers/participantCtrl');


module.exports = function(app) {
    app.post('/participant', participantCtl.create)
    app.get('/participant', participantCtl.list)
    app.get('/participant/:pid', participantCtl.getItem)
};