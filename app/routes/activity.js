var activityCtl = require('../controllers/activityCtrl');


module.exports = function(app) {
    app.post('/activity', activityCtl.create)
    app.get('/activity', activityCtl.list)
    app.get('/activity/:aid', activityCtl.getItem)
    app.get('/activity/event/:uid/:eid', activityCtl.getEvents)
};