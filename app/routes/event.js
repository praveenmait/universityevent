var eventCtl = require('../controllers/eventCtrl');


module.exports = function(app) {
    app.post('/events', eventCtl.create)
    app.get('/events', eventCtl.list)
    app.get('/events/:eid', eventCtl.getItem)
};