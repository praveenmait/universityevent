'use strict';

var passport = require('passport'),
	loginCtl = require('../controllers/login');

module.exports = function(app) {
  /**
   * Login/Registration : Local
  */
  app.post('/user/login', loginCtl.loginLocal);
  app.post('/user/register', loginCtl.registerLocal);

  app.post('/users/logout', loginCtl.signout);
};