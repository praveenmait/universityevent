'use strict';

/**
 * Module dependencies.
 */
var chai = require('chai'),
    should = chai.should(),
    supertest = require('supertest'),
    config = require('../../config/env/test'),
    api = supertest('http://localhost:' + config.port),
    async = require('async'),
    mongoose = require('mongoose');

chai.use(require('chai-things'));




// Login Function
var login = function(api, email, password, callback) {
    var agent = require('superagent').agent();
    api.post('/user/login')
        .send({
            email: email,
            password: password
        })
        .expect(200)
        // .expect('Content-Type', /json/)
        .end(function(err, res) {
            if (err) return callback(err);
            agent.saveCookies(res);
            callback(null, agent, res);
        });
};

// Register
var register = function(api, user, callback) {
    api.post('/user/register')
        .send(user)
        .expect(200)
        // .expect('Content-Type', /json/)
        .end(function(err, res) {
            if (err) return callback(err);
            callback(null, res);
        });
};



describe('UnitTesting', function() {


    // within before() you can run all the operations that are needed to setup your tests. In this case
    // I want to create a connection with the database, and when I'm done, I call done().
    var coninja = null;
    var agent = require('superagent').agent();

    var user1, user2;
    var cookieAgent, cookieAgent2;
    var productId, commentId, collectionId;
    describe('Users', function(){

        before(function(done) {
            // Register two users & login with first
            register(api, {
                email: "varun2@rubird.com",
                password: "spiderman",
                name: "varun"
            }, function(err, res) {
                if (err) return done(err);
                user1 = JSON.parse(res.text);

                register(api, {
                    email: "qwert20992@gmail.com",
                    password: "spiderman",
                    name: "varun2",
                }, function(err, res) {
                    if (err) return done(err);
                    user2 = JSON.parse(res.text);

                    // Logging in
                    login(api, "varun2@rubird.com", "spiderman", function(err, agent, res) {
                        if (err) return done(err);
                        cookieAgent = agent;
                        login(api, "qwert20992@gmail.com", "spiderman", function(err, agent, res) {
                            if (err) return done(err);
                            cookieAgent2 = agent;
                            done();
                        });
                    });
                })
            })
        });


        it("should be able to login", function(done) {

            api
                .post('/user/login')
                .send({
                    email: "varun2@rubird.com",
                    password: "spiderman"
                })
                .expect(200)
                // .expect('Content-Type', /json/)
                .end(function(err, res) {
                    if (err) return done(err);
                    done();
                });
        });


        it("should be to register", function(done) {
            api
                .post('/user/register')
                .send({
                    email: "varun@rubird.com",
                    password: "spiderman",
                    user_name: "varun",
                    company_name: "rubird",
                    job_title: "test"

                })
                .expect(200)
                .expect('Content-Type', /json/)
                .end(function(err, res) {
                    if (err) return done(err);
                    coninja = JSON.parse(res.text);
                    // coninja.profile_image.should.match(/^http/i);
                    agent.saveCookies(res);
                    done();
                });
        });


        it('should correctly update an existing account', function(done){
            api
                .put("/users/" + user2._id)
                
                .send({username : 'praveen',
                    company_name : 'test technology',
                    job_title : 'test title',
                    email : 'testemail@gmail.com'})

                .expect(200) // status code
                .end(function(err,res){
                    if(err) {
                        console.log(err);
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    res.body['response'].should.have.property('_id');
                    res.body['response'].should.have.property('user_name').equal('praveen');
                    res.body['response'].should.have.property('company_name').equal('test technology');
                    res.body['response'].should.have.property('job_title').equal('test title'); 
                    res.body['response'].should.have.property('email').equal('testemail@gmail.com'); 
                    res.body['response'].should.not.equal(null);


                    done();
                });
        });
        

        // after(function (done) {
        //    var db = require('mongoose').connect(config.db, function() {
        //                 // Cleaning up database
        //                 db.connection.db.dropDatabase();
        //                 db.connection.close();
        //                 done();
        //             });
        //     });
    });

    describe('Products',function(){


        before(function(done) {
            // Register two users & login with first
            register(api, {
                email: "varun2@rubird.com",
                password: "spiderman",
                name: "varun"
            }, function(err, res) {
                if (err) return done(err);
                user1 = JSON.parse(res.text);

                register(api, {
                    email: "qwert20992@gmail.com",
                    password: "spiderman",
                    name: "varun2",
                }, function(err, res) {
                    if (err) return done(err);
                    user2 = JSON.parse(res.text);

                    // Logging in
                    login(api, "varun2@rubird.com", "spiderman", function(err, agent, res) {
                        if (err) return done(err);
                        cookieAgent = agent;
                        login(api, "qwert20992@gmail.com", "spiderman", function(err, agent, res) {
                            if (err) return done(err);
                            cookieAgent2 = agent;
                            done();
                        });
                    });
                })
            })
        });
        
        
        it('get list of product', function(done){
            var req = api
                .get("/products");
            cookieAgent.attachCookies(req);
            req
                .send({searchText : 'test'})
                .expect(200) // status code
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }
                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    res.body.should.have.property('products');
                    res.body.should.have.property('message');
                    res.body.should.have.property('resultMessage');

                    done();
                });
        });

        it('create product', function(done){
            var req= api
                .post("/products")
            cookieAgent.attachCookies(req);

            req
                .send({product_url:"www.anasdc.com",
                       product_name:"testaspadasdasd name",
                       tagline:"test tasdfsdfsdfssdfgline"})

                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    productId=res.body['response']['_id']
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('product is successfully posted');

                    done();
                });
        });

        it('get product items', function(done){
            var req= api
                .get("/products/"+productId)
            cookieAgent.attachCookies(req);

            req
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }
                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('product is successfully fetched');
                    done();
                });
        });

        it('vote on product items', function(done){
            
            var req = api
                .put("/products/"+productId +"/vote");
            cookieAgent.attachCookies(req);
            req
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        console.log(err);
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage');

                    done();
                });
        });

        it('update product items', function(done){
            var req= api
                .put("/products/"+productId)
            cookieAgent.attachCookies(req);

            req
                .send({product_url:"www.alksd.com",
                       product_name:"testasd name",
                       tagline:"test tagasdline"})
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied
                                        
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('product is successfully updated');

                    done();
                });
        });


        it('comment on product items', function(done){
            var req= api
                .post("/products/"+productId+"/comments")
            cookieAgent.attachCookies(req);

            req
                .send({comment:"This is the testing comment"})
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied
                    commentId=res.body['result'][0]['comments']['_id']
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('Successfully commented');
                    res.body.should.have.property('result');
                    done();
                });
        });


        it('vote on comment of product items', function(done){
            var req= api
                // .post("/products/543f6952b468485003f67e70/543f6952b468485003f67e71/vote")
                .post("/products/543fcc96fed4f2ae1058c640/543fcc96fed4f2ae1058c641/vote")
            cookieAgent.attachCookies(req);

            req
                // .send({comment:"This is the testing comment"})
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied
                    commentId=res.body['result'][0]['comments'][0]['_id']
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('Successfully voted');
                    res.body.should.have.property('result');
                    done();
                });
        });

        it('delete product items', function(done){
            var req= api
                .delete("/products/"+productId)
            cookieAgent.attachCookies(req);

            req
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }
                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied
                                        
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('product is successfully deleted');
                    done();
                });
        });



    });

// test cases for the collections of the products

    describe('ProductsCollections',function(){

        // test case for get list of collection using searchtext

        it('get list of collection/productlist', function(done){
            var req = api
                .get("/lists?searchText=test");
            cookieAgent.attachCookies(req);
            req
                .expect(200) // status code
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }
                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied
                    res.body.should.have.property('result');
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('Collections is successfully fetched');

                    done();
                });
        }); 

        // test case for creating collection

        it('create collection', function(done){
            var req= api
                .post("/lists")
            cookieAgent.attachCookies(req);

            req
                .send({title:"testing collection title",
                       description:"test description",
                       tagline:"test tasdfsdfsdfssdfgline"})

                .expect(200) // status code
                // .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied
                    
                    collectionId=res.body['result']['_id']
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('Collection is successfully created');

                    done();
                });

        });

        // collection item details

        it('get collection items', function(done){
            var req= api
                .get("/lists/"+collectionId)
            cookieAgent.attachCookies(req);

            req
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }
                    
                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('Collection detail is successfully fetched');
                    done();
                });
        });


        it('update collection items', function(done){
            var req= api
                .put("/lists/"+collectionId)
            cookieAgent.attachCookies(req);

            req
                .send({title:"updated tested collection",
                       description:"updated description name",
                       image:"asdasdasdasdasd",
                       tagline:"updated tagline for the collection"})
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied
                    res.body.should.have.property('result')
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('Collection is successfully updated');

                    done();
                });
        });
        
        it('follow the collections', function(done){
            var req = api
                .put("/lists/" + collectionId +"/follow");
            cookieAgent.attachCookies(req);
            req
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        console.log(err);
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage');

                    done();
                });
        });

        it('vote on collection items', function(done){
            
            var req = api
                .put("/lists/"+collectionId +"/vote");
            cookieAgent.attachCookies(req);
            req
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        console.log(err);
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage');

                    done();
                });
        });

        it('approval for the suggested products', function(done){
            
            var req = api
                .put("/lists/"+collectionId +"/approval");
            cookieAgent.attachCookies(req);
            req
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        console.log(err);
                        return done(err);
                    }

                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied

                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('approval complete');

                    done();
                });
        });

        it('delete collections items', function(done){
            var req= api
                .delete("/lists/"+collectionId)
            cookieAgent.attachCookies(req);

            req
                .expect(200) // status code
                .expect('Content-Type', /json/)
                .end(function(err,res){
                    if(err) {
                        return done(err);
                    }
                    // this is should.js syntax, very clear
                    // Should.js fluent syntax applied
                                        
                    res.body.should.have.property('message').equal('success');
                    res.body.should.have.property('resultMessage').equal('Collection is successfully deleted');
                    done();
                });
        });


    });
    
});
