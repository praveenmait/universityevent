var mongoose = require('mongoose'),
    async = require('async'),
    config = require('../../config/config'),
    Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    Server = require('mongodb').Server,
    Participants = mongoose.model('Participants'),
    _ = require('lodash');


exports.create = function(req, res, next) {
    var newParticipant =  new Participants(req.body)
    newParticipant.save(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
          })
    })
};


exports.list = function(req, res, next) {
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Participants.find({})
    .populate('institute')
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        Participants.count({})
        .exec(function(errcnt, cnt){
            if(errcnt) return next({
                error: errcnt,
                code: 400
            })   
            res.json({
                'response': data,
                'page': page,
                'perPage': perPage,
                'totalItems' : cnt
            })
        })
    })
};


exports.getItem = function(req, res, next) {
    Participants.findById(req.params.pid)
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })  
        res.json({
            'response': data,
        })
    })
};



exports.del = function(req, res, next) {
    participantDetails = req.params;
    if (_.isUndefined(participantDetails.eid)) {
        participantDetails.cid = req.user._id;
    }
    Participants.remove({
      '_id': participantDetails.eid
    },
    function(err, data) {
      if (err)
        return res.send(400, {
            message: getErrorMessage(err)
        });
      res.jsonp({
        'message': 'success',
        'resultMessage': 'Participant details deleted successfully',
        'results': data
      })
    })

}
