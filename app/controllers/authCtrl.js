var _ = require('lodash');
// Checks if user is logged in and check the userType
exports.requestChecker = function(allowedRoles){
    return function(req, res, next) {
        if (req.user){
            if (req.user.userType==1)
                return next({
                    code: 403,
                    message: "CAN READ HOME PAGE ONLY"
                })
            else if(req.user.userType==2)
                return next({
                    code: 403,
                    message: "CAN READ FILES BUT NOT DOWNLOAD"
                })
            else if(req.user.userType==4)
                return next();
        }
        else{
            return next({
                code: 403,
                message: "NOT AUTHORIZED"
            });
        }
    };
}