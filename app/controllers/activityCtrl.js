var mongoose = require('mongoose'),
    async = require('async'),
    config = require('../../config/config'),
    Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    Server = require('mongodb').Server,
    Activity = mongoose.model('Activity'),
    _ = require('lodash');


exports.create = function(req, res, next) {
    var newActivity =  new Activity(req.body)
    newActivity.save(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
          })
    })
};


exports.list = function(req, res, next) {
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Activity.find({})
    .populate('university')
    .populate('events')
    .populate('participants')
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        Activity.count({})
        .exec(function(errcnt, cnt){
            if(errcnt) return next({
                error: errcnt,
                code: 400
            })   
            res.json({
                'response': data,
                'page': page,
                'perPage': perPage,
                'totalItems' : cnt
            })
        })
    })
};


exports.getItem = function(req, res, next) {
    Activity.findById(req.params.aid)
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })  
        res.json({
            'response': data,
        })
    })
};

exports.getEvents = function(req, res, next) {
    var queryObj = {
        university: req.params.uid
    };
    if(!_.isEmpty(req.params.eid)){
        queryObj.events=req.params.eid;
    }
    Activity.find(queryObj)
    .populate('university')
    .populate('events')
    .populate('participants')
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })  
        res.json({
            'response': data,
        })
    })
};
