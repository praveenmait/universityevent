var mongoose = require('mongoose'),
    async = require('async'),
    config = require('../../config/config'),
    Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    Server = require('mongodb').Server,
    University = mongoose.model('University'),
    _ = require('lodash');


exports.create = function(req, res, next) {
    var newUniversity =  new University(req.body)
    newUniversity.save(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
          })
    })
};


exports.list = function(req, res, next) {
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    University.find({})
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        University.count({})
        .exec(function(errcnt, cnt){
            if(errcnt) return next({
                error: errcnt,
                code: 400
            })   
            res.json({
                'response': data,
                'page': page,
                'perPage': perPage,
                'totalItems' : cnt
            })
        })
    })
};


exports.getItem = function(req, res, next) {
    University.findById(req.params.uid)
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })  
        res.json({
            'response': data,
        })
    })
};

