var mongoose = require('mongoose'),
    async = require('async'),
    config = require('../../config/config'),
    Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    Server = require('mongodb').Server,
    Event = mongoose.model('Event'),
    _ = require('lodash');


exports.create = function(req, res, next) {
    var newEvent =  new Event(req.body)
    newEvent.save(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
          })
    })
};


exports.list = function(req, res, next) {
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Event.find({})
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        Event.count({})
        .exec(function(errcnt, cnt){
            if(errcnt) return next({
                error: errcnt,
                code: 400
            })   
            res.json({
                'response': data,
                'page': page,
                'perPage': perPage,
                'totalItems' : cnt
            })
        })
    })
};


exports.getItem = function(req, res, next) {
    console.log('req.params',req.params);
    Event.findById(req.params.eid)
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })  
        res.json({
            'response': data,
        })
    })
};
