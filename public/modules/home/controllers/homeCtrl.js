angular.module('main-app')

.controller('homeCtrl',['$scope', '$http', '$state', 'homeSrvc', 
	function($scope, $http, $state, homeSrvc){
	$scope.activity={};	
	$scope.activities =[];
	$scope.eventList=[];
	$scope.universityList=[];
	$scope.activityList =[];
	homeSrvc.listEvents(function(data){
		$scope.eventList = angular.copy(data.data.response);
	});
	homeSrvc.listUniversity(function(data){
		$scope.universityList = angular.copy(data.data.response);
	});
	homeSrvc.getActivityList(function(data){
		$scope.activityList = angular.copy(data.data.response);
	});
	$scope.getEvents = function(){
		var queryData={
			uid:$scope.activity.university,
			eid:$scope.activity.events
		};
		homeSrvc.getEvents(queryData, function(data){
			$scope.activities =angular.copy(data.data);
		});
	};
}])