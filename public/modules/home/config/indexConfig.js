angular.module('main-app')

.config(['stateHelperProvider', '$locationProvider', '$urlRouterProvider', '$cookiesProvider', '$sceProvider',
  function(stateHelperProvider, $locationProvider, $urlRouterProvider, $cookiesProvider, $sceProvider){
  // console.log($sceProvider)
  // $sceProvider.enabled(false);
  $locationProvider.html5Mode(true);
  // $urlRouterProvider.otherwise('/');
  stateHelperProvider

  .setNestedState({
            name: "index",
            url: '/',
            templateUrl: "templates/modules/home/views/header",
            controller: "homeCtrl"
        })
}])
