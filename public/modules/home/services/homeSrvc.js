angular.module('main-app')


.factory('homeSrvc', ['$http' , function($http){
    return{
        listUniversity: function(cb){
            return $http.get('/university')
            .then(function(data){
                cb(data);
            })
        },
        listEvents: function(cb){
            return $http.get('/events')
            .then(function(data){
                cb(data);
            })
        },
        getDetail: function(cb){
            return $http.get('/participants')
            .then(function(data){
                cb(data);
            })  
        },
        getActivityList: function(cb){
            return $http.get('/activity')
            .then(function(data){
                cb(data);
            })  
        },
        getEvents: function(data, cb){
            return $http.get('/activity/event/'+data.uid+'/'+data.eid)
            .then(function(data){
                cb(data);
            })  
        }
    }

}])