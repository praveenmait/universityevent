angular.module('admin-app')

.factory('eventSrvc', ['$http', 'growl', 
  function($http, growl){
    return {
      create: function(params, cb){
        console.log('srvc',params);
        return $http.post('/events', params)
        .then(function(data){
          cb(data);
        })
      },
      list: function(cb){
        return $http.get('/events')
        .then(function(data){
            cb(data);
          })
      },
      getItem: function(params, cb){
        return $http.get('/events/'+params.eid)
            .then(function(data){
              console.log('data getItem srvc',data);
              cb(data.data.response);
          })
      }       
    } 
  }])