angular.module('admin-app')

.factory('activitySrvc', ['$http', 'growl', 
  function($http, growl){
    return {
      create: function(params, cb){
        console.log('srvc',params);
        return $http.post('/activity', params)
        .then(function(data){
          cb(data);
        })
      },
      list: function(cb){
        return $http.get('/activity')
        .then(function(data){
            cb(data);
          })
      },
      getItem: function(params, cb){
        return $http.get('/activity/'+params.aid)
            .then(function(data){
              console.log('data getItem srvc',data);
              cb(data.data.response);
          })
      }       
    } 
  }])