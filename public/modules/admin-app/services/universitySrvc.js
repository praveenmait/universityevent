angular.module('admin-app')

.factory('universitySrvc', ['$http', 'growl', 
  function($http, growl){
    return {
      create: function(params, cb){
        return $http.post('/university',params)
        .then(function(data){
          cb(data);
        })
      },
      list: function(cb){
        return $http.get('/university')
        .then(function(data){
            cb(data);
          })
      },
      getItem: function(params, cb){
        return $http.get('/university/'+params)
            .then(function(data){
              cb(data.data.response);
          })
      }       
    } 
  }])