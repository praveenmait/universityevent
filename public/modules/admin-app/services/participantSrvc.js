angular.module('admin-app')

.factory('participantSrvc', ['$http', 'growl', 
  function($http, growl){
    return {
      create: function(params, cb){
        return $http.post('/participant', params)
        .then(function(data){
          cb(data);
        })
      },
      list: function(cb){
        return $http.get('/participant')
        .then(function(data){
            cb(data);
          })
      },
      getItem: function(params, cb){
        return $http.get('/participant/'+params)
            .then(function(data){
              cb(data.data.response);
          })
      }       
    } 
  }])