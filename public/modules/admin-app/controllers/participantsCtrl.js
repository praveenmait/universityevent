angular.module('admin-app')


.controller('participantCtrl', ['$http', '$scope', '$state', 'participantSrvc', 'universitySrvc',
    function($http, $scope, $state, participantSrvc, universitySrvc) {
        $scope.participants={};
        $scope.participantList =[];
        $scope.universityList =[];
        $scope.saveParticipant = function() {
            console.log('participants save data',$scope.participants);
            participantSrvc.create($scope.participants, function(data) {
                $state.go('participants.list');
            });
        };
    	participantSrvc.list(function(data){
    		$scope.participantList = angular.copy(data.data);
    	});
        universitySrvc.list(function(data){
            console.log('universityList',data);
            $scope.universityList = angular.copy(data.data.response);
        });
        $scope.getItem = function(pid){
            $state.go('participants.getItem',{'pid':pid});
        };
        if($state.params.pid){
            participantSrvc.getItem($state.params.pid, function(data){
                $scope.participants = angular.copy(data);
            });
        }
    }
])