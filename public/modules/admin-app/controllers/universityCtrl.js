angular.module('admin-app')



.controller('universityCtrl', ['$http', '$scope', '$state', 'universitySrvc', 'eventSrvc',
    function($http, $scope, $state, universitySrvc, eventSrvc) {
        $scope.university={};
        $scope.universityList =[];
        $scope.saveUniversity = function() {
            universitySrvc.create($scope.university, function(data) {
                $state.go('university.list');
            });
        };
    	universitySrvc.list(function(data){
    		$scope.universityList = angular.copy(data.data);
    	});
        eventSrvc.list(function(data){
            $scope.eventList = angular.copy(data.data.response);
        });
        $scope.getItem = function(uid){
        	$state.go('university.getItem',{'uid':uid});
        };
        if($state.params.uid){
            universitySrvc.getItem($state.params.uid, function(data){
                $scope.university = angular.copy(data);
            });
        }
    }
])