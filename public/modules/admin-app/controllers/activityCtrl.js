angular.module('admin-app')



.controller('activityCtrl', ['$http', '$scope', '$state', 'eventSrvc', 'activitySrvc', 'universitySrvc', 'participantSrvc',
    function($http, $scope, $state, eventSrvc, activitySrvc, universitySrvc, participantSrvc) {
    	$scope.activity={};
        $scope.activityList =[];
        $scope.eventList=[];
        $scope.universityList =[];
        $scope.participantList =[];
        $scope.saveActivity = function() {
            activitySrvc.create($scope.activity, function(data) {
                $state.go('activity.list');
            });
        };
    	activitySrvc.list(function(data){
    		$scope.activityList = angular.copy(data.data);
    	});
        eventSrvc.list(function(data){
            $scope.eventList = angular.copy(data.data.response);
        });
        universitySrvc.list(function(data){
            $scope.universityList = angular.copy(data.data.response);
        });
        participantSrvc.list(function(data){
            $scope.participantList = angular.copy(data.data.response);
        });
        $scope.getItem = function(aid){
            $state.go('activity.getItem',{'aid':aid});
        };
        if($state.params.aid){
            activitySrvc.getItem({aid:$state.params.aid}, function(data){
                $scope.activity = data;
                $scope.activity.startDate = new Date($scope.activity.startDate);
                $scope.activity.endDate = new Date($scope.activity.endDate);
            });
        }
    }
])