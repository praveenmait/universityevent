angular.module('admin-app')



.controller('eventCtrl', ['$http', '$scope', '$state', 'eventSrvc',
    function($http, $scope, $state, eventSrvc) {
    	$scope.Event={};
        $scope.eventList =[];
        $scope.saveEvent = function() {
            eventSrvc.create($scope.Event, function(data) {
                $state.go('adminhome.list');
            });
        };
    	eventSrvc.list(function(data){
    		$scope.eventList = angular.copy(data.data);
    	});
        $scope.getItem = function(eid){
            $state.go('adminhome.getItem',{'eid':eid});
        };
        if($state.params.eid){
            eventSrvc.getItem({eid:$state.params.eid}, function(data){
                $scope.Event = data;
                $scope.Event.startime = new Date($scope.Event.startime);
                $scope.Event.endtime = new Date($scope.Event.endtime);
            });
        }
    }
])