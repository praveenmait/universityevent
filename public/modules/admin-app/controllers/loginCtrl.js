angular.module('admin-app')



.controller('adminLoginCtrl', ['$http', '$scope', '$state', 'growl',
    function($http, $scope, $state, growl) {
        $scope.user = {};
        $scope.login = function() {
            $scope.user.type=1;
            $http.post('/user/login', $scope.user)
                .then(function(data) {
                    if (data.error) {
                        var message = data.error.message
                        alert(message)
                    }
                    console.log('data',data);
                    $state.go('adminhome.list');
                },function(data) {
                    alert("error", data)
                });
        }
        $scope.logout = function(){
            $http.post('/users/logout')
                .then(function(data) {
                  delete window.__user
                  $state.go("login");
            })
        }
    }
])