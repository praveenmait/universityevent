angular.module('admin-app')

.config(function(stateHelperProvider, $locationProvider, $urlRouterProvider){
  $locationProvider.html5Mode(true);
  // $urlRouterProvider.otherwise('/index/list');
  stateHelperProvider

  .setNestedState({
            name: "login",
            url: '/admin',
            templateUrl: "templates/modules/admin-app/views/login",
            controller: "adminLoginCtrl",
            module: 'public',
        })
    .setNestedState({
            name: "adminhome",
            url: '/index',
            templateUrl: "templates/modules/admin-app/views/universityAdmin",
            controller: "adminLoginCtrl",
            // abstract: true,
            // module: 'public',
            children: [{
                name: "list",
                url: '/list?page&perPage&q',
                views: {
                    "AdminView": {
                        templateUrl: "templates/modules/admin-app/views/list",
                        controller: "eventCtrl",
                    }
                }
                },{
                    name: "create",
                    url: '/create?eid',
                    views: {
                        "AdminView": {
                            templateUrl: "templates/modules/admin-app/views/createEvent",
                            controller: "eventCtrl",
                        }
                    }
                },{
                    name: "getItem",
                    url: '/event?eid',
                    views: {
                        "AdminView": {
                            templateUrl: "templates/modules/admin-app/views/createEvent",
                            controller: "eventCtrl",
                        }
                    }
                }]
        })
    .setNestedState({
            name: "participants",
            url: '/participant',
            templateUrl: "templates/modules/admin-app/views/universityAdmin",
            controller: "adminLoginCtrl",
            // abstract: true,
            // module: 'public',
            children: [{
                name: "list",
                url: '/list?page&perPage&q',
                views: {
                    "AdminView": {
                        templateUrl: "templates/modules/admin-app/views/participantList",
                        controller: "participantCtrl",
                    }
                }
                },{
                    name: "create",
                    url: '/create?pid',
                    views: {
                        "AdminView": {
                            templateUrl: "templates/modules/admin-app/views/createParticipants",
                            controller: "participantCtrl",
                        }
                    }
                },{
                    name: "getItem",
                    url: '/participants?pid',
                    views: {
                        "AdminView": {
                            templateUrl: "templates/modules/admin-app/views/createParticipants",
                            controller: "participantCtrl",
                        }
                    }
                }]
        })
    .setNestedState({
            name: "university",
            url: '/university',
            templateUrl: "templates/modules/admin-app/views/universityAdmin",
            controller: "adminLoginCtrl",
            // abstract: true,
            // module: 'public',
            children: [{
                name: "list",
                url: '/list?page&perPage&q',
                views: {
                    "AdminView": {
                        templateUrl: "templates/modules/admin-app/views/universityList",
                        controller: "universityCtrl",
                    }
                }
                },{
                    name: "create",
                    url: '/create?uid',
                    views: {
                        "AdminView": {
                            templateUrl: "templates/modules/admin-app/views/createUniversity",
                            controller: "universityCtrl",
                        }
                    }
                },{
                    name: "getItem",
                    url: '/university?uid',
                    views: {
                        "AdminView": {
                            templateUrl: "templates/modules/admin-app/views/createUniversity",
                            controller: "universityCtrl",
                        }
                    }
                }]
        })
    .setNestedState({
            name: "activity",
            url: '/activity',
            templateUrl: "templates/modules/admin-app/views/universityAdmin",
            controller: "adminLoginCtrl",
            // abstract: true,
            // module: 'public',
            children: [{
                name: "list",
                url: '/list?page&perPage&q',
                views: {
                    "AdminView": {
                        templateUrl: "templates/modules/admin-app/views/activityList",
                        controller: "activityCtrl",
                    }
                }
                },{
                    name: "create",
                    url: '/create?aid',
                    views: {
                        "AdminView": {
                            templateUrl: "templates/modules/admin-app/views/createActivity",
                            controller: "activityCtrl",
                        }
                    }
                },{
                    name: "getItem",
                    url: '/university?aid',
                    views: {
                        "AdminView": {
                            templateUrl: "templates/modules/admin-app/views/createActivity",
                            controller: "activityCtrl",
                        }
                    }
                }]
        })
})
